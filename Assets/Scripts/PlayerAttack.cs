﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//********************************************************************************
//                                 Authors                                       *
//                                                                               *
//                              Lukas Schuster                                   *
//                                    +                                          *
//                             Daniel Mielnicki: Shoot, Object-Pooling           *    
//********************************************************************************

[RequireComponent(typeof(AudioSource))]
public class PlayerAttack : MonoBehaviour
{
    private const float TimeToResetBools = 0.5f;

    public LayerMask ToHitMask;

    public float TackerDelay = 0.5f;
    public float SwordDelay = 0.2f;

    public int SwordDamage;
    public int TackerDamage;

    public float SwordReach = 0.5f;

    [Header("Sound")]
    private AudioSource myAudio;
    public AudioClip ShootSound;

    [Header("Animation")]
    public GameObject SlashObject;

    [Header("Tacker")]
    public GameObject Bullet;
    public float BulletForce;
    public Transform BulletSpawnPoint;

    public int PooledAmount = 20;
    private List<GameObject> bullets;

    private PlayerController2D controller;
    private GameManager manager;
    private Health myHealth;

    private bool canShoot = true;
    private bool canSwordAttack = true;

    private float shootTimer;
    private float swordTimer;

    void Awake()
    {
        controller = GetComponent<PlayerController2D>();
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Start()
    {
        myHealth = controller.gameObject.GetComponent<Health>();

        bullets = new List<GameObject>();
        for (int i = 0; i < PooledAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(Bullet);
            obj.SetActive(false);
            bullets.Add(obj);
        }

        myAudio = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (manager.Gametime != 1 || myHealth.CurrentHealth <= 0)
            return;

        UpdateTimer();
        GetInput();
    }

    private void UpdateTimer()
    {
        swordTimer += Time.deltaTime;
        shootTimer += Time.deltaTime;

        canShoot = shootTimer >= TackerDelay ? true : false;
        canSwordAttack = swordTimer >= SwordDelay ? true : false;
    }

    private void GetInput()
    {
        if (Input.GetKey(GameStats.Attack))
        {
            switch (PlayerPrefs.GetString("currentWeapon"))
            {
                case "Cutter":
                    if (canSwordAttack)
                        SwordAttack();
                    break;
                case "Tacker":
                    if (canShoot)
                        Shoot();
                    break;
                case "Standard":
                    break;
                default:
                    break;
            }
        }
    }

    private void Shoot()
    {
        if (!controller.collisionBelow)
            return;

        if (PlayerPrefs.GetInt("Ammo") > 0)
            PlayerPrefs.SetInt("Ammo", PlayerPrefs.GetInt("Ammo") - 1);
        else
            return;

        shootTimer = 0;

        controller.animator.SetBool("attack", true);
        StartCoroutine(BoolReseter());

        for (int i = 0; i < bullets.Count; i++)
        {
            //Looking for an inactive bullet
            if (!bullets[i].activeInHierarchy)
            {
                //get player pos
                bullets[i].transform.position = BulletSpawnPoint.transform.position;

                //set active
                bullets[i].SetActive(true);
                bullets[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(controller.Direction * BulletForce, 0));
                break;
            }
        }
        if(myAudio.isPlaying)
        {
            myAudio.Stop();
            myAudio.PlayOneShot(ShootSound);
        }
        myAudio.PlayOneShot(ShootSound);

    }

    private void SwordAttack()
    {
        swordTimer = 0;

        controller.animator.SetBool("attack", true);
        StartCoroutine(BoolReseter());

        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right * controller.Direction, SwordReach, ToHitMask);

        if (!hit)
            return;

        if (hit.transform.gameObject.GetComponent<Health>() != null)
        {
            Instantiate(SlashObject, hit.transform.position, Quaternion.identity);

            Health otherHealth = hit.transform.gameObject.GetComponent<Health>();
            otherHealth.RemoveHealth(SwordDamage);
        }
    }

    IEnumerator BoolReseter()
    {
        yield return new WaitForSeconds(TimeToResetBools);
        controller.animator.SetBool("attack", false);
    }
}
