﻿using UnityEngine;
using UnityEngine.UI;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class Currency : MonoBehaviour
{
    public Text MoneyText;

    void Start()
    {
        if (MoneyText == null)
            MoneyText = GameObject.Find("HudCanvas/Image/MoneyText").GetComponent<Text>();

        GameStats.Money = PlayerPrefs.GetInt("money");
        MoneyText.text = GameStats.Money.ToString();
    }

    public void RemoveMoney(int money)
    {
        GameStats.Money -= money;

        if (GameStats.Money < 0)
            GameStats.Money = 0;

        MoneyText.text = GameStats.Money.ToString();
        PlayerPrefs.SetInt("money", GameStats.Money);
    }

    public bool CheckForMoney(int money)
    {
        return GameStats.Money >= money ? true : false;
    }

    public void AddMoney(int money)
    {
        GameStats.Money += money;
        MoneyText.text = GameStats.Money.ToString();
        PlayerPrefs.SetInt("money", GameStats.Money);
    }
}
