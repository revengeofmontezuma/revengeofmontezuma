﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class Wobbling : MonoBehaviour
{
    public float Length;
    public float Smoothness;
    public Vector3 Direction;

    [Tooltip("Change Direction when this near to the WantedPosition")]
    public float MaxDistanceToWantedPos = 2;

    public bool StartRandom = false;

    private GameManager manager;
    private Vector3 startPosition;
    private Vector3 targetPosition;
    private Vector3 wantedPosition;

    private bool useManager = false;
    private float delay;
    private float timer;

    void Awake()
    {
        if (GameObject.Find("GameManager") != null)
        {
            manager = GameObject.Find("GameManager").GetComponent<GameManager>();
            useManager = true;
        }

        if (StartRandom)
            delay = Random.Range(0.1f, 1f);

        startPosition = transform.localPosition;
        targetPosition = transform.localPosition + Direction;
        wantedPosition = targetPosition;
    }

    void Update()
    {
        if (StartRandom)
        {
            timer += Time.deltaTime;
            if (timer >= delay)
                StartRandom = false;
            else
                return;
        }

        if (useManager)
        {
            if (manager.Gametime != 0)
                MoveImage();
            else
                return;
        }
        else
            MoveImage();
    }

    private void MoveImage()
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, wantedPosition, Smoothness);

        if (Mathf.Abs(transform.localPosition.x - wantedPosition.x) <= MaxDistanceToWantedPos && Mathf.Abs(transform.localPosition.y - wantedPosition.y) <= MaxDistanceToWantedPos)
            wantedPosition = startPosition;

        if (Mathf.Abs(transform.localPosition.x - wantedPosition.x) <= MaxDistanceToWantedPos && Mathf.Abs(transform.localPosition.y - wantedPosition.y) <= MaxDistanceToWantedPos)
            wantedPosition = targetPosition;
    }
}
