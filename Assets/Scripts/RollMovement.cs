﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                             Daniel Mielnicki                                  *    
//********************************************************************************

public class RollMovement : MonoBehaviour {

    public float Speed = 1;
    public int Direction = -1;

    private Transform player;

    //Distance defines when player gets damage
    public float SetDistanceDmg = 1.3f;

    private string playerName = "Player_Normal";


    void Start()
    {
        player = GameObject.Find(playerName).transform;
    }

    void FixedUpdate()
    {
        gameObject.transform.Translate(new Vector3(Speed / 100 * Direction, 0));
    }

    void Update()
    {
        Explosion();
    }

    void Explosion()
    {
        float distance = Vector2.Distance(player.position, gameObject.transform.position);

        if(distance <= SetDistanceDmg)
        {
            player.gameObject.GetComponent<Health>().RemoveHealth(100);
            Destroy(gameObject);
        }
    }
}
