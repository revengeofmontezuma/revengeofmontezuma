﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

[RequireComponent(typeof(BoxCollider2D))]
public class EnemyController : MonoBehaviour
{
    private const float hollowWidth = 0.015f;

    public enum ObjectType { Enemy_1 = 1, Enemy_2 = 2, Enemy_3 = 3, Enemy_4 = 4}
    public ObjectType PawnType;

    [Tooltip("LayerMask To Collider with")]
    public LayerMask CollisionMask;

    public LayerMask PlayerMask;

    public AudioSource EnemyDeathSound;

    public float WatchDistance;
    public float AttackDistance;
    public int DistanceToIgnore = 15;
    public float TimeToDeleteOnDeath;

    [Header("Movement")]
    [Range(3, 10)]
    public int Speed = 25;
    public float JumpHeight = 3;
    public float JumpTime = 0.4f;

    public float AccelarationGrounded = 0.1f;
    public float AccelarationInAir = 0.3f;

    public float MaxHeightToTarget = 0.5f;

    [Header("Collision")]
    public int VerticalRayCount = 2;
    public int HorizontalRayCount = 2;

    [Header("Attack")]
    public float AttackDelay;

    [HideInInspector]
    public int Direction = -1;

    [HideInInspector]
    public bool Walking;

    private Transform target;
    private GameManager manager;
    private Animator animator;
    private BoxCollider2D myCollider;
    private Health myHealth;

    private Vector3 velocity;

    private bool collisionAbove = false;
    private bool collisionBelow = false;
    private bool collisionRight = false;
    private bool collisionLeft = false;

    private Vector2 topLeft;
    private Vector2 bottomLeft;
    private Vector2 topRight;
    private Vector2 bottomRight;

    private float RayHorizontalSpace;
    private float RayVerticalSpace;

    private float jumpPower;
    private float gravity;
    private float velocityXSmoothing;

    private bool canAttack = true;
    private float attackTimer;

    private bool dead = false;

    void Awake()
    {
        target = GameObject.Find("Player_Normal").GetComponent<Transform>();
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        animator = GetComponent<Animator>();
        myCollider = GetComponent<BoxCollider2D>();
        myHealth = GetComponent<Health>();
    }

    void Start()
    {
        GetRaySpace();

        gravity = -(2 * JumpHeight) / Mathf.Pow(JumpTime, 2);
        jumpPower = Mathf.Abs(gravity) * JumpTime;

        myHealth.OnDeath += MyHealth_OnDeath;
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, target.position) >= DistanceToIgnore)
        {
            animator.enabled = false;
            return;
        }

        if (dead)
            return;

        UpdateTimer();
        UpdateAnimations();

        if (manager.Gametime != 0)
        {
            UpdateDirection();
            CheckDistance();
        }

        CheckForHeadAttack();
    }

    private void MyHealth_OnDeath()
    {
        dead = true;
        animator.SetBool("dead", true);
        StartCoroutine(DeathTimer());

        if (EnemyDeathSound != null && !EnemyDeathSound.isPlaying)
            EnemyDeathSound.Play();
    }

    IEnumerator DeathTimer()
    {
        yield return new WaitForSeconds(TimeToDeleteOnDeath);
        Destroy(gameObject);
    }

    private void CheckForHeadAttack()
    {
        if (PawnType == ObjectType.Enemy_1)
        {
            if (target.position.x < myCollider.bounds.max.x && target.position.x > myCollider.bounds.min.x)
            {
                if (Mathf.Abs(target.position.y - transform.position.y) > 2.8 && Mathf.Abs(target.position.y - transform.position.y) < 3.2)
                {
                    if (target.position.y > transform.position.y)
                    {
                        myHealth.RemoveHealth(1000);
                        target.gameObject.GetComponent<PlayerController2D>().Jump();
                    }
                }
            }
        }
    }

    private void UpdateDirection()
    {
        Direction = target.position.x <= transform.position.x ? 1 : -1;
        transform.localScale = new Vector3(Direction, transform.localScale.y, transform.localScale.z);
    }

    private void UpdateAnimations()
    {
        Walking = false;
        animator.SetBool("walking", false);
        animator.enabled = manager.Gametime == 1 ? true : false;
    }

    private void CheckDistance()
    {
        if (Vector2.Distance(target.position, transform.position) <= WatchDistance)
            CheckCanSeeTarget();
        else
            ApplyGravity();
    }

    private void UpdateTimer()
    {
        if (canAttack == false)
        {
            attackTimer += Time.deltaTime;
            if (attackTimer >= AttackDelay)
            {
                canAttack = true;
                attackTimer = 0;
            }
        }
    }

    private void CheckCanSeeTarget()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, GetPlayerDirectionVector(), Vector2.Distance(target.position, transform.position), PlayerMask);

        if (hit.transform == target)
        {
            if (Vector2.Distance(transform.position, target.position) >= AttackDistance)
                MakeDecision();
            else
            {
                if (canAttack)
                    AttackTarget();
            }
        }
    }

    private void ApplyGravity()
    {
        if (collisionAbove || collisionBelow)
            velocity.y = 0;

        velocity.y += gravity * Time.deltaTime;
        velocity.x = 0;

        Move(velocity * Time.deltaTime);
    }

    private void MakeDecision()
    {
        if (collisionAbove || collisionBelow)
            velocity.y = 0;


        Walking = true;
        animator.SetBool("walking", true);

        if (collisionLeft || collisionRight)
        {
            animator.SetBool("walking", false);
            Walking = false;
        }


        velocity.y += gravity * Time.deltaTime;

        if (Mathf.Abs(transform.position.y - target.position.y) < MaxHeightToTarget)
        {
            float wantedVelocityX = Direction * Speed;
            velocity.x = -wantedVelocityX;
        }
        else
        {
            velocity.x = 0;
            Walking = false;
            animator.SetBool("walking", false);
        }

        Move(velocity * Time.deltaTime);
    }

    private Vector2 GetPlayerDirectionVector()
    {
        Vector2 tempVector = -((transform.position - target.position).normalized);
        return tempVector;
    }

    private void Move(Vector3 velocity)
    {
        UpdateRayCastsOrigins();
        ResetCollisionBools();

        if (velocity.x != 0)
            VerticalCollision(ref velocity);

        if (velocity.y != 0)
            HorizontalCollision(ref velocity);

        transform.Translate(velocity);
    }

    private void GetRaySpace()
    {
        Bounds bounds = myCollider.bounds;
        bounds.Expand(hollowWidth * -2);

        HorizontalRayCount = Mathf.Clamp(HorizontalRayCount, 2, int.MaxValue);
        VerticalRayCount = Mathf.Clamp(VerticalRayCount, 2, int.MaxValue);

        RayHorizontalSpace = bounds.size.x / (HorizontalRayCount - 1);
        RayVerticalSpace = bounds.size.y / (VerticalRayCount - 1);
    }

    private void UpdateRayCastsOrigins()
    {
        Bounds bounds = myCollider.bounds;
        bounds.Expand(hollowWidth * -2);

        topLeft = new Vector2(bounds.min.x, bounds.max.y);
        bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        topRight = new Vector2(bounds.max.x, bounds.max.y);
        bottomRight = new Vector2(bounds.max.x, bounds.min.y);
    }

    private void ResetCollisionBools()
    {
        collisionAbove = false;
        collisionBelow = false;
        collisionRight = false;
        collisionLeft = false;
    }

    private void HorizontalCollision(ref Vector3 velocity)
    {
        float directionY = Mathf.Sign(velocity.y);
        float rayLength = Mathf.Abs(velocity.y) + hollowWidth;

        for (int i = 0; i < HorizontalRayCount; i++)
        {
            Vector2 rayOrigin = new Vector2();

            if (directionY == -1)
                rayOrigin = bottomLeft;
            else
                return;

            rayOrigin += Vector2.right * (RayHorizontalSpace * i + velocity.x);

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, CollisionMask);

            Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);

            if (hit)
            {
                velocity.y = (hit.distance - hollowWidth) * directionY;
                rayLength = hit.distance;

                if (directionY == -1)
                    collisionBelow = true;

                if (directionY == 1)
                    collisionAbove = true;
            }
        }
    }

    private void VerticalCollision(ref Vector3 velocity)
    {
        float directionX = Mathf.Sign(velocity.x);
        float rayLength = Mathf.Abs(velocity.x) + hollowWidth;

        for (int i = 0; i < VerticalRayCount; i++)
        {
            Vector2 rayOrigin = new Vector2();

            if (directionX == -1)
                rayOrigin = bottomLeft;
            else
                rayOrigin = bottomRight;

            rayOrigin += Vector2.up * (RayVerticalSpace * i);

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, CollisionMask);

            Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);

            if (hit)
            {
                float hitAngle = Vector2.Angle(hit.normal, Vector2.up);

                velocity.x = (hit.distance - hollowWidth) * directionX;
                rayLength = hit.distance;

                if (directionX == -1)
                    collisionLeft = true;

                if (directionX == 1)
                    collisionRight = true;
            }
        }
    }

    private void AttackTarget()
    {
        Health playerHealth = target.GetComponent<Health>();
        playerHealth.RemoveHealth(1000);
        canAttack = false;
    }
}
