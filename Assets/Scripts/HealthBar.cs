﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class HealthBar : MonoBehaviour
{
    public Slider HealthbarSlider;

    private Health bossHealth;

    void Awake()
    {
        bossHealth = GetComponent<Health>();
    }

    void Start()
    {
        bossHealth.OnHealthChanged += BossHealth_OnHealthChanged;
        bossHealth.OnDeath += BossHealth_OnDeath;
    }

    private void BossHealth_OnDeath()
    {
        HealthbarSlider.gameObject.SetActive(false);
    }

    private void BossHealth_OnHealthChanged()
    {
        HealthbarSlider.value = (float)bossHealth.CurrentHealth / 100;

        if(HealthbarSlider.value == 0)
            HealthbarSlider.gameObject.SetActive(false);
    }
}
