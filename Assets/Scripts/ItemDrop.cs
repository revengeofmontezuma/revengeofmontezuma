﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

[RequireComponent(typeof(Health))]
public class ItemDrop : MonoBehaviour
{
    public GameObject ToDrop;

    private Health myhealth;

    void Awake()
    {
        myhealth = GetComponent<Health>();
    }

    void Start()
    {
        if (ToDrop == null)
            throw new MissingReferenceException("ToDrop is not set!");

        myhealth.OnDeath += Myhealth_OnDeath;
    }

    private void Myhealth_OnDeath()
    {
        StartCoroutine(SpawnTimer());
    }

    IEnumerator SpawnTimer()
    {
        yield return new WaitForSeconds(1);
        Instantiate(ToDrop, transform.position, Quaternion.identity);
    }
}
