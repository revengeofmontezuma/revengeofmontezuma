﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class HudScript : MonoBehaviour
{
    public Health Playerhealth;

    public Sprite TackerSprite;
    public Sprite CutterSprite;

    public Image Heart1;
    public Image Heart2;
    public Image Heart3;

    public Text AmmoText;

    public Image ImageToControll;

    private bool tacker = false;

    private Color Transparent;

    void Awkake()
    {
        if (TackerSprite == null || CutterSprite == null)
            throw new MissingReferenceException("Give Reference to TackerSprite and CutterSprite");

        Playerhealth = GameObject.Find("Player_Normal").GetComponent<Health>();
    }

    void Start()
    {
        Transparent = new Color(0, 0, 0, 0);

        Playerhealth.OnHealthChanged += Playerhealth_OnHealthChanged;
        Playerhealth_OnHealthChanged();

        switch (PlayerPrefs.GetString("currentWeapon"))
        {
            case "Cutter":
                ImageToControll.sprite = CutterSprite;
                ImageToControll.rectTransform.sizeDelta = new Vector2(85, ImageToControll.rectTransform.sizeDelta.y);
                ImageToControll.rectTransform.localPosition = new Vector3(-357.5f, ImageToControll.rectTransform.localPosition.y, ImageToControll.rectTransform.localPosition.z);
                break;
            case "Tacker":
                ImageToControll.sprite = TackerSprite;
                tacker = true;
                break;
            default:
                ImageToControll.color = Transparent;
                break;
        }
    }

    void Update()
    {
        if (tacker)
            AmmoText.text = PlayerPrefs.GetInt("Ammo").ToString();
    }

    private void Playerhealth_OnHealthChanged()
    {
        switch (PlayerPrefs.GetInt("ExtraHeart"))
        {
            case 0:
                ResetAllheartExept(new List<Image>() { });
                break;
            case 1:
                ResetAllheartExept(new List<Image>() { Heart1 });
                break;
            case 2:
                ResetAllheartExept(new List<Image>() { Heart1, Heart2 });
                break;
            case 3:
                ResetAllheartExept(new List<Image>() { Heart1, Heart2, Heart3 });
                break;
        }
    }

    private void ResetAllheartExept(List<Image> hearts)
    {
        Color showColor = Heart1.color;


        Color heart1Color = Heart1.color;
        heart1Color.a = 0;
        Heart1.color = heart1Color;

        Color heart2Color = Heart2.color;
        heart2Color.a = 0;
        Heart2.color = heart2Color;

        Color heart3Color = Heart3.color;
        heart3Color.a = 0;
        Heart3.color = heart3Color;


        foreach (var image in hearts)
            image.color = showColor;
    }
}
