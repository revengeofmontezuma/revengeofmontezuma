﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                             Daniel Mielnicki                                  *    
//********************************************************************************

public class DestroyBullet : MonoBehaviour {

    public float LifeTime = 2f;

    void OnEnable()
    {
        Invoke("Destroy", LifeTime);
    }

    void Destroy()
    {
        gameObject.SetActive(false);
    }

    void OnDisable()
    {
        //For safty to prevent double disable on gameObjects
        CancelInvoke();
    }
}
