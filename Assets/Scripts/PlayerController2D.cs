﻿using System;
using UnityEngine;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

[RequireComponent(typeof(BoxCollider2D))]
public class PlayerController2D : MonoBehaviour 
{
    // Units the Raycasts start within the bounds of the collider
    private const float hollowWidth = 0.015f;

    [Header("Sound")]
    public AudioSource JumpSound;
    public AudioSource DeathFart;
    public AudioSource DeathSound;

    [Header("UI")]
    public Canvas PauseCanvas;
    public Canvas HudCanvas;

    [Header("Movement")]
    public float MoveSpeed = 6;

    public float AccelarationGrounded = 0.1f;
    public float AccelarationInAir = 0.3f;

    public float JumpHeight = 3;
    public float JumpTime = 0.4f;

    public float MaxAngleToClimb = 60;

    private float jumpPower;
    private float gravity;
    private float velocityXSmoothing;

    [Header("Collision")]
    public int VerticalRayCount = 2;
    public int HorizontalRayCount = 2;

    public LayerMask Mask;

    [HideInInspector]
    public BoxCollider2D myCollider;

    [HideInInspector]
    public Vector3 velocity;

    [HideInInspector]
    public Animator animator;

    private Vector2 topLeft;
    private Vector2 bottomLeft;
    private Vector2 topRight;
    private Vector2 bottomRight;

    [HideInInspector]
    public bool collisionAbove = false;
    [HideInInspector]
    public bool collisionBelow = true;
    [HideInInspector]
    public bool collisionRight = false;
    [HideInInspector]
    public bool collisionLeft = false;

    [HideInInspector]
    public bool Walking;

    [HideInInspector]
    public int Direction = 1;

    private float RayHorizontalSpace;
    private float RayVerticalSpace;

    private GameManager gameManager;
    private Health myHealth;

    void Awake()
    {
        myCollider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        myHealth = GetComponent<Health>();
        myHealth.OnDeath += MyHealth_OnDeath;
    }

    private void MyHealth_OnDeath()
    {
        Walking = false;
        animator.SetBool("dead", true);
        animator.SetBool("idle", false);
        animator.SetBool("falling", false);
        animator.SetBool("jumping", false);
        animator.SetBool("running", false);

        DeathFart.PlayDelayed(1.1f);
        DeathSound.Play();
    }

    void Start() 
    {
        GetRaySpace();
        GetCurrentWeapon();

        gravity = -(2 * JumpHeight) / Mathf.Pow(JumpTime, 2);
        jumpPower = Mathf.Abs(gravity) * JumpTime;
	}
	
	void Update() 
    {
        if (!myHealth.Alive)
        {
            ApplyGravity();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            gameManager.Gametime = gameManager.Gametime == 1 ? 0 : 1;

        UpdateTimeMechanics();

        if (gameManager.Gametime != 0)
        {
            GetInput();
            ChooseDirection();
        }

        UpdateAnimations();
	}

    public void Jump()
    {
        velocity.y = jumpPower;
        if (!JumpSound.isPlaying)
            JumpSound.Play();
    }

    private void UpdateAnimations()
    {

        if (Input.GetKey(GameStats.Left) || Input.GetKey(GameStats.Right))
        {
            if (collisionBelow)
            {
                Walking = true;
                animator.SetBool("running", true);
            }

            animator.SetBool("idle", false);
        }
        else
        {
            if (collisionBelow)
                animator.SetBool("idle", true);

            animator.SetBool("running", false);
            Walking = false;
        }

        if (Input.GetKey(GameStats.Jump) && velocity.y >= 0.2)
        {
            animator.SetBool("jumping", true);
            animator.SetBool("running", false);
            Walking = false;
            animator.SetBool("idle", false);
        }
        else
        {
            animator.SetBool("jumping", false);
            if (!collisionBelow)
                animator.SetBool("falling", true);
        }
        if (collisionBelow)
            animator.SetBool("falling", false);
    }

    private void GetCurrentWeapon()
    {
        switch (PlayerPrefs.GetString("currentWeapon"))
        {
            case "Standard":
                animator.SetBool("Standard", true);
                return;
            case "Cutter":
                animator.SetBool("Cutter", true);
                return;
            case "Tacker":
                animator.SetBool("Tacker", true);
                return;
            default:
                animator.SetBool("Standard", true);
                return;
        }
    }

    private void UpdateTimeMechanics()
    {
        PauseCanvas.enabled = gameManager.Gametime == 1 ? false : true;

        HudCanvas.enabled = gameManager.Gametime == 1 ? true : false;

        animator.enabled = gameManager.Gametime == 1 ? true : false;
    }

    private void ChooseDirection()
    {
        transform.localScale = new Vector3(velocity.x < 0 ? -1 : 1,transform.localScale.y, transform.localScale.z);
        Direction = transform.localScale.x == -1 ? -1 : 1;
    }

    private void GetInput()
    {
        if (collisionAbove || collisionBelow)
            velocity.y = 0;

        float inputX = 0;
        inputX -= Input.GetKey(GameStats.Right) ? 1 : 0;
        inputX -= Input.GetKey(GameStats.Left) ? -1 : 0;

        float inputY = 0;
        inputY += Input.GetKey(GameStats.Jump) ? 1 : 0;

        Vector2 input = new Vector2(inputX, inputY);

        if (collisionBelow && Input.GetKey(GameStats.Jump))
        {
            Jump();
        }

        float wantedVelocityX = input.x * MoveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, wantedVelocityX, ref velocityXSmoothing, (collisionBelow) ? AccelarationGrounded : AccelarationInAir);
        velocity.y += gravity * Time.deltaTime;

        if (float.IsNaN(velocity.x) || float.IsNaN(velocity.y) ||float.IsNaN(velocity.z))
        {
            print("Error FLoats Are NaN, Check Public Variables!");
            print("X: " + velocity.x + " Y: " + velocity.y + " Z: " + velocity.z);
            return;
        }

        Move(velocity * Time.deltaTime);
    }

    private void ApplyGravity()
    {
        if (collisionAbove || collisionBelow)
            velocity.y = 0;

        velocity.y += gravity * Time.deltaTime;
        velocity.x = 0;

        Move(velocity * Time.deltaTime);
    }

    private void UpdateRayCastsOrigins()
    {
        Bounds bounds = myCollider.bounds;
        bounds.Expand(hollowWidth * -2);

        topLeft = new Vector2(bounds.min.x, bounds.max.y);
        bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        topRight = new Vector2(bounds.max.x, bounds.max.y);
        bottomRight = new Vector2(bounds.max.x, bounds.min.y);
    }

    private void GetRaySpace()
    {
        Bounds bounds = myCollider.bounds;
        bounds.Expand(hollowWidth * -2);

        HorizontalRayCount = Mathf.Clamp(HorizontalRayCount, 2, int.MaxValue);
        VerticalRayCount = Mathf.Clamp(VerticalRayCount, 2, int.MaxValue);

        RayHorizontalSpace = bounds.size.x / (HorizontalRayCount - 1);
        RayVerticalSpace = bounds.size.y / (VerticalRayCount - 1);
    }

    private void Move(Vector3 velocity)
    {
        UpdateRayCastsOrigins();
        ResetCollisionBools();

        if (velocity.x != 0)
            VerticalCollision(ref velocity);

        if (velocity.y != 0)
            HorizontalCollision(ref velocity);

        transform.Translate(velocity);
    }

    private void HorizontalCollision(ref Vector3 velocity)
    {
        float directionY = Mathf.Sign(velocity.y);
        float rayLength = Mathf.Abs(velocity.y) + hollowWidth;

        for (int i = 0; i < HorizontalRayCount; i++)
        {
            Vector2 rayOrigin = new Vector2();

            if (directionY == -1)
                rayOrigin = bottomLeft;
            else
                return;
                // Removed because we can not Jump Upwards through Obstacles
                // rayOrigin = topLeft;

            rayOrigin += Vector2.right * (RayHorizontalSpace * i + velocity.x);

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, Mask);

            Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);

            if (hit)
            {
                velocity.y = (hit.distance - hollowWidth) * directionY;
                rayLength = hit.distance;

                if (directionY == -1)
                    collisionBelow = true;

                if (directionY == 1)
                    collisionAbove = true;
            }
        }
    }

    private void VerticalCollision(ref Vector3 velocity)
    {
        float directionX = Mathf.Sign(velocity.x);
        float rayLength = Mathf.Abs(velocity.x) + hollowWidth;

        for (int i = 0; i < VerticalRayCount; i++)
        {
            Vector2 rayOrigin = new Vector2();

            if (directionX == -1)
                rayOrigin = bottomLeft;
            else
                rayOrigin = bottomRight;

            rayOrigin += Vector2.up * (RayVerticalSpace * i);

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, Mask);

            Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);

            if (hit)
            {
                velocity.x = (hit.distance - hollowWidth) * directionX;
                rayLength = hit.distance;

                if (directionX == -1)
                    collisionLeft = true;

                if (directionX == 1)
                    collisionRight = true;
            }
        }
    }

    private void GetUpSlope(ref Vector3 velocity, float angle)
    {
        float distanceToMove = Mathf.Abs(velocity.x);

        velocity.x = Mathf.Cos(angle * Mathf.Deg2Rad) * distanceToMove * Mathf.Sign(velocity.x);
        velocity.y = Mathf.Sign(angle * Mathf.Deg2Rad) * distanceToMove;
    }

    private void ResetCollisionBools()
    {
        collisionAbove = false;
        collisionBelow = false;
        collisionRight = false;
        collisionLeft = false;
    }
}
