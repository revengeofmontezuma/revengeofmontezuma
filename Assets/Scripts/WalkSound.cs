﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class WalkSound : MonoBehaviour
{
    public AudioSource WalkAudio;

    public enum ObjectType { Player = 1, Enemy = 2};
    public ObjectType SelectedType;

    private EnemyController eController;
    private PlayerController2D pController;

    private GameManager manager;

    void Awake()
    {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();

        if (SelectedType == ObjectType.Enemy)
        {
            eController = GetComponent<EnemyController>();
            return;
        }

        if (SelectedType == ObjectType.Player)
        {
            pController = GetComponent<PlayerController2D>();
            return;
        }
    }

    void Update()
    {
        if (manager.Gametime != 0)
        {
            switch (SelectedType)
            {
                case ObjectType.Player:
                    CheckWalking(ObjectType.Player);
                    break;
                case ObjectType.Enemy:
                    CheckWalking(ObjectType.Enemy);
                    break;
                default:
                    throw new System.Exception("No Type Selected in Component");
            }
        }
    }

    void CheckWalking(ObjectType type)
    {
        if (type == ObjectType.Player)
        {
            if (pController.Walking)
            {
                if (!WalkAudio.isPlaying)
                    WalkAudio.Play();
            }
            else
                WalkAudio.Stop();
            return;
        }

        if (type == ObjectType.Enemy)
        {
            if (eController.Walking)
            {
                if (!WalkAudio.isPlaying)
                    WalkAudio.Play();
            }
            else
                WalkAudio.Stop();
            return;
        }
    }
}
