﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class BackGroundScrolling : MonoBehaviour
{
    [Tooltip("First Layer in List Shows nearest")]
    public List<BackGroundLayer> Layers = new List<BackGroundLayer>();

    public float MaxPositionY = 6f;
    public float MinPositionY = 4.4f;

    [Tooltip("The Distance the player can move away from the middle layer without changing the Order, Leave this at Value 12!")]
    public float MaxDistance = 12f;

    private PlayerController2D player;
    private Health playerhealth;
    private GameManager gameManager;

    private float spriteWidth;

    void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Start()
    {
        player = GetComponent<PlayerController2D>();
        playerhealth = player.GetComponent<Health>();

        spriteWidth = Layers[0].GetComponent<SpriteRenderer>().bounds.max.x * 2;

        for (int i = 0; i < Layers.Count; i++)
        {
            Layers[i].FillDictionary();
            for (int number = -1; number < Layers[i].Images.Count - 1; number++)
            {
                if (number == -1)
                {
                    Layers[i].Images[number + 1] = Instantiate(Layers[i].Images[number + 1], new Vector3(player.transform.position.x - 19.2f, player.transform.position.y, Layers[i].PositionZ), Quaternion.identity) as GameObject;
                    Layers[i].Left = Layers[i].Images[number + 1];
                }
                if (number == 0)
                {
                    Layers[i].Images[number + 1] = Instantiate(Layers[i].Images[number + 1], new Vector3(player.transform.position.x, player.transform.position.y, Layers[i].PositionZ), Quaternion.identity) as GameObject;
                    Layers[i].Middle = Layers[i].Images[number + 1];
                }
                if (number == 1)
                {
                    Layers[i].Images[number + 1] = Instantiate(Layers[i].Images[number + 1], new Vector3(player.transform.position.x + 19.2f, player.transform.position.y, Layers[i].PositionZ), Quaternion.identity) as GameObject;
                    Layers[i].Right = Layers[i].Images[number + 1];
                }
            }
        }
    }

    void Update()
    {
        if (playerhealth.CurrentHealth <= 0)
            return;

        SetLayerPosition();
        UpdateCurrentLayer();
    }

    private void UpdateCurrentLayer()
    {
        for (int i = 0; i < Layers.Count; i++)
        {
            if (Vector2.Distance(player.transform.position, Layers[i].Middle.transform.position) > MaxDistance)
            {
                if (player.transform.position.x > Layers[i].Middle.transform.position.x)
                {
                    Layers[i].Left.transform.position = Layers[i].Right.transform.position + new Vector3(spriteWidth / 4, 0, 0);

                    GameObject tempLeft = Layers[i].Left;
                    GameObject tempMiddle = Layers[i].Middle;
                    GameObject tempRight = Layers[i].Right;

                    Layers[i].Middle = tempRight;
                    Layers[i].Left = tempMiddle;
                    Layers[i].Right = tempLeft;
                }
                else
                {
                    Layers[i].Right.transform.position = Layers[i].Left.transform.position - new Vector3(spriteWidth / 4, 0, 0);

                    GameObject tempLeft = Layers[i].Left;
                    GameObject tempMiddle = Layers[i].Middle;
                    GameObject tempRight = Layers[i].Right;

                    Layers[i].Middle = tempLeft;
                    Layers[i].Left = tempRight;
                    Layers[i].Right = tempMiddle;
                }
            }
        }
    }

    private void SetLayerPosition()
    {
        if (player.collisionLeft == false && player.collisionLeft == false)
        {
            for (int i = 0; i < Layers.Count; i++)
            {
                foreach (var item in Layers[i].Images)
                {
                    if (item.Value == null || Layers[i].Images == null)
                        continue;

                    item.Value.transform.Translate(new Vector3(-player.velocity.x * Layers[i].Speed.x * Time.deltaTime * gameManager.Gametime, player.collisionBelow ? 0 : -player.velocity.y * Layers[i].Speed.y * Time.deltaTime * gameManager.Gametime, 0));

                    Vector3 tempVector = item.Value.transform.position;
                    tempVector.y = Mathf.Clamp(item.Value.transform.position.y, MinPositionY, MaxPositionY);

                    item.Value.transform.position = tempVector;
                }
            }
        }
    }
}
