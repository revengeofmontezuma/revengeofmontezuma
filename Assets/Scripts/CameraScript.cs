﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class CameraScript : MonoBehaviour 
{
    public PlayerController2D target;
    public Vector2 FreespaceSize;
    public float FollowSpeed = 0.05f;
    public float CamHeight;
    [Tooltip("How far the Camera Looks forward in the Direction the Player looks")]
    public float FrontLook = 3.5f;

    [Header("Limitations")]
    public Vector2 MaxPosition;
    public Vector2 MinPosition;

    private Bounds boundsOfTarget;

    private Vector2 velocity;
    private Vector2 center;
    private float top, bottom, left, right;

    private Vector3 wantedPosition;

    void Start()
    {
        Bounds targetBounds = target.myCollider.bounds;

        top = targetBounds.min.y + FreespaceSize.y;
        bottom = targetBounds.min.y;
        left = targetBounds.center.x - FreespaceSize.x / 2;
        right = targetBounds.center.x + FreespaceSize.x / 2;

        velocity = Vector2.zero;
        center = new Vector2((left + right) / 2, (top + bottom) / 2);
    }

    void LateUpdate()
    {
        boundsOfTarget = target.myCollider.bounds;
        
        UpdateFreeSpace();

        wantedPosition = transform.position;
        wantedPosition = center;
        wantedPosition.z = target.transform.position.z - 5;
        wantedPosition.y += CamHeight;
        wantedPosition.x += FrontLook * target.Direction;

        Vector3 position = Vector3.Lerp(transform.position, wantedPosition, FollowSpeed);

        if (position.x > MaxPosition.x || position.x < MinPosition.x || position.y > MaxPosition.y || position.y < MinPosition.y)
            return;

        transform.position = position;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.2f);
        Gizmos.DrawCube(center, FreespaceSize);
    }

    private void UpdateFreeSpace()
    {
        float moveX = 0;

        if (boundsOfTarget.min.x < left)
            moveX = boundsOfTarget.min.x - left;
        else if (boundsOfTarget.max.x > right)
            moveX = boundsOfTarget.max.x - right;

        left += moveX;
        right += moveX;

        float moveY = 0;

        if (boundsOfTarget.min.y < bottom)
            moveY = boundsOfTarget.min.y - bottom;
        else if (boundsOfTarget.max.y > top)
            moveY = boundsOfTarget.max.y - top;

        bottom += moveY;
        top += moveY;

        center = new Vector2((left + right) / 2, (bottom + top) / 2);
        velocity = new Vector2(moveX, moveY);
    }
}
