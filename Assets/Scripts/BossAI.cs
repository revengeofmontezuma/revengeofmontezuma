﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                             Daniel Mielnicki                                  *    
//********************************************************************************

public class BossAI : MonoBehaviour
{

    [Header("Attack")]
    public GameObject Target;
    public float EnemySight = 10;
    public int MyAttacksCount;
    public float AttackCD;
    public Animator BossAnimator;

    public AudioSource RollSound;
    public AudioSource MudSound;
    public AudioSource BossSpawnSound;

    [Header("Corrections")]
    [Header("Roll")]
    public GameObject Roll;
    public float RollSpawnX = -2.3f;
    public float RollSpawnY = -1.8f;
    public float RollSpawnTime = 0.8f;
    [Header("Shit")]
    public GameObject Shit;
    public float ShitSpawnX = 0.15f;
    public float ShitSpawnY = 2.6f;
    public float ShitSpawnTime = 0.7f;
    [Header("General")]
    public float SpawnZ = -10;

    public float SpawnBoss;
    private bool alive = true;

    private Transform player;
    private GameManager manager;
    private Health bossHealth;
    private float cooldown;

    private string attack1 = "Attack1";
    private string attack2 = "Attack2";
    private string idle = "Idle";

    private string animNameAttack1 = "Boss_Attack_1_Anim";
    private string animNameAttack2 = "Boss_Attack_2_Anim";
    private string animNameIdle = "Boss_Idle_Anim";

    void Start()
    {
        player = Target.GetComponent<Transform>();
        bossHealth = GetComponent<Health>();
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();

        bossHealth.OnDeath += BossHealth_OnDeath;

        gameObject.SetActive(false);

        Invoke("SpawnMyBoss", SpawnBoss);
    }

    private void BossHealth_OnDeath()
    {
        alive = false;
        BossAnimator.SetTrigger("Death");
    }

    void Update()
    {
        if (manager.Gametime != 1 || alive != true)
            return;

        if (Vector2.Distance(player.transform.position, gameObject.transform.position) < EnemySight)
        {
            BossAnimator.SetBool(idle, false);

            int num = Random.Range(1, MyAttacksCount + 1);
            if (num == 1 && Time.time >= cooldown && !BossAnimator.GetCurrentAnimatorStateInfo(0).IsName(attack1))
            {
                Attack1();
                if (!MudSound.isPlaying)
                    MudSound.Play();
            }
            else if (num == 2 && Time.time >= cooldown && !BossAnimator.GetCurrentAnimatorStateInfo(0).IsName(attack2))
            {
                Attack2();
                if (!RollSound.isPlaying)
                    RollSound.Play();
            }
        }
        else
        {
            Idle();
        }
    }

    void Idle()
    {
        BossAnimator.Play(animNameIdle);
    }

    void Attack1()
    {
        cooldown = Time.time + AttackCD;
        BossAnimator.SetTrigger(attack1);
        Invoke("SpawnShit", ShitSpawnTime);
    }

    void Attack2()
    {
        cooldown = Time.time + AttackCD;
        BossAnimator.SetTrigger(attack2);
        Invoke("SpawnRoll", RollSpawnTime);
    }

    void SpawnShit()
    {
        Instantiate(Shit, new Vector3(gameObject.transform.position.x + ShitSpawnX, gameObject.transform.position.y + ShitSpawnY, SpawnZ), Quaternion.identity);
    }

    void SpawnRoll()
    {
        Instantiate(Roll, new Vector3(gameObject.transform.position.x + RollSpawnX, gameObject.transform.position.y + RollSpawnY, SpawnZ), Quaternion.identity);
    }

    void SpawnMyBoss()
    {
        gameObject.SetActive(true);
        BossSpawnSound.Play();
    }
}
