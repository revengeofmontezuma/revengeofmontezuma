﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                             Daniel Mielnicki                                  *    
//********************************************************************************

[RequireComponent(typeof(AudioSource))]
public class BulletDetection : MonoBehaviour
{

    public int DamageBullet;
    public GameObject ExplosionParticle;
    public AudioClip ExplosionSound;
    private AudioSource myAudio;

    void Start()
    {
        myAudio = GetComponent<AudioSource>();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<Health>().RemoveHealth(DamageBullet);
            if(myAudio.isPlaying)
            {
                myAudio.Stop();
                myAudio.PlayOneShot(ExplosionSound);
            }
            myAudio.PlayOneShot(ExplosionSound);
            StartCoroutine(SetInactive(1));
        }

        else if(other.transform.gameObject.tag == "Boss")
        {
            other.gameObject.GetComponent<Health>().RemoveHealth(DamageBullet);
        }

        else if (other.transform.gameObject.tag != "Player")
        {
            Instantiate(ExplosionParticle, gameObject.transform.position, Quaternion.identity);
            if (myAudio.isPlaying)
            {
                myAudio.Stop();
                myAudio.PlayOneShot(ExplosionSound);
            }
            myAudio.PlayOneShot(ExplosionSound);
            StartCoroutine(SetInactive(1));
        }
    }

    IEnumerator SetInactive(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }
}
