﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class ChangeCurrentSprite : MonoBehaviour
{
    public Transform Player;
    public Sprite OverLaySprite;
    public float TimeToShow;

    private Sprite standardSprite;
    private SpriteRenderer spriteRenderer;

    private bool isShowing = false;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        standardSprite = spriteRenderer.sprite;
    }

    void Update()
    {
        if (Player.position.x <= 0 && !isShowing)
        {
            spriteRenderer.sprite = OverLaySprite;
            isShowing = true;

            StartCoroutine(Timer());
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(TimeToShow);

        if (Player.position.x <= 0)
            StartCoroutine(Timer());

        isShowing = false;
        spriteRenderer.sprite = standardSprite;
    }
}
