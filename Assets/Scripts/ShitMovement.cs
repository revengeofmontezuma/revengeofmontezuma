﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                             Daniel Mielnicki                                  *    
//********************************************************************************

public class ShitMovement : MonoBehaviour {

    public float BossForce = 50;

    public GameObject ShitPrefab;

    private Transform player;
    private string objectToFind= "Player_Normal";

    private Rigidbody2D rig;
    private Collider2D myCollider2D;

    void Start()
    {
        player = (Transform)GameObject.Find(objectToFind).transform;

        rig = GetComponent<Rigidbody2D>();

        myCollider2D = GetComponent<Collider2D>();

        Shoot();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(other.gameObject.GetComponent<Health>().CurrentHealth > 0)
            {
                other.gameObject.GetComponent<Health>().RemoveHealth(1000);
                Destroy(gameObject);
            }
        }

        if (other.gameObject.tag == "Floor")
        {
            Instantiate(ShitPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    void Shoot()
    {
        float distance = GetDistance(gameObject.transform.position.x, player.position.x);
        rig.AddForce(new Vector2(distance * BossForce * -1, 0));
    }

    float GetDistance(float point, float point2)
    {
        return (point - point2);
    }
}
