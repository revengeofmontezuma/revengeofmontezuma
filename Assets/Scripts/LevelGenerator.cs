﻿using UnityEngine;
using System.Collections.Generic;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class LevelGenerator : MonoBehaviour
{
    [Header("Level Part Prefabs")]
    public GameObject EndPart;
    public GameObject StartBackGround;

    public List<GameObject> LevelParts = new List<GameObject>();
    public List<GameObject> HellLevelParts = new List<GameObject>();

    public int LevelLength;

    public Sprite BackGround1;
    public Sprite BackGround2;
    public Sprite EndBackGround;

    [Header("ToiletPaper Placement")]
    public GameObject PaperPrefab;

    [Tooltip("On the Sprites in the List, can Spawn Toiletpaper")]
    public List<Sprite> ToPlaceToiletPaperOn = new List<Sprite>();

    [Range(0, 100)]
    public int PaperDensity;

    [Header("Enmey Placement")]
    public GameObject Enemy1;
    public GameObject Enemy2;
    public GameObject Enemy3;
    public GameObject Enemy4;
    [Range(0, 100)]
    public int EnemyDensity;

    [Tooltip("On the Sprites in the List, can Spawn Enemys")]
    public List<Sprite> ToPlaceEnemysOn = new List<Sprite>();

    private GameObject lastObject;
    private Vector3 lastObjectDownRight;
    private GameObject currentAddedPart;

    private GameObject lastPartForCheck = null;

    private int partCounter = 0;

    void Start()
    {
        lastObject = StartBackGround;
        lastObjectDownRight = new Vector3(lastObject.gameObject.GetComponent<SpriteRenderer>().bounds.max.x, lastObject.gameObject.GetComponent<SpriteRenderer>().bounds.min.y, lastObject.transform.position.z);

        GenerateWorld();
    }

    private void GenerateWorld()
    {
        for (int i = 0; i < LevelLength; i++)
        {
            int rndIndex = Random.Range(0, LevelParts.Count);

            if (i == LevelLength - 1)
                InstantiatePart(EndPart);
            else
            {
                GameObject part = null;

                if (i >= LevelLength / 2)
                {
                    while (part == lastPartForCheck || part == null)
                        part = HellLevelParts[Random.Range(0, HellLevelParts.Count)];

                    InstantiatePart(part);
                }
                else
                {

                    while (part == lastPartForCheck || part == null)
                        part = LevelParts[Random.Range(0, LevelParts.Count)];

                    InstantiatePart(part);
                }
            }

            partCounter++;
        }
    }

    private void InstantiatePart(GameObject part)
    {
        GameObject newPart = (GameObject)Instantiate(part, transform.position, Quaternion.identity);
        currentAddedPart = newPart;
        MoveToCorrectPlace(ref newPart);

        currentAddedPart = lastPartForCheck;
    }

    private Vector3 GetLastGameObjectPositionDownRight(GameObject part)
    {
        if (part == StartBackGround)
            return new Vector3(StartBackGround.GetComponent<SpriteRenderer>().bounds.max.x, StartBackGround.GetComponent<SpriteRenderer>().bounds.min.y, StartBackGround.transform.position.z);

        SpriteRenderer[] childs = new SpriteRenderer[100];
        List<SpriteRenderer> backGrounds = new List<SpriteRenderer>();

        childs = part.GetComponentsInChildren<SpriteRenderer>();

        foreach (SpriteRenderer child in childs)
        {
            if (child.sprite == BackGround1 || child.sprite == BackGround2 || child.sprite == EndBackGround)
                backGrounds.Add(child);

            if (ToPlaceToiletPaperOn.Contains(child.sprite))
            {
                if (Random.Range(0, 100) <= PaperDensity)
                {
                    Vector3 spawnPos = Vector3.zero;

                    switch (child.sprite.ToString())
                    {
                        case "lamp2 (UnityEngine.Sprite)":
                            spawnPos = child.transform.position + new Vector3(0, 3, 0);
                            SetZPositionTo(ref spawnPos, -2);
                            break;
                        case "Tisch (UnityEngine.Sprite)":
                            spawnPos = child.transform.position + new Vector3(0.4f, -2.2f, 0);
                            SetZPositionTo(ref spawnPos, -2);
                            break;
                        case "watercooler (UnityEngine.Sprite)":
                            spawnPos = child.transform.position + new Vector3(0.33f, 2.7f, 0);
                            SetZPositionTo(ref spawnPos, -2);
                            break;
                        case "office_cube2 (UnityEngine.Sprite)":
                            spawnPos = child.transform.position + new Vector3(3.1f, -0.8f, 0);
                            SetZPositionTo(ref spawnPos, -2);
                            break;
                    }

                    if (spawnPos == Vector3.zero)
                       throw new System.Exception("Spawn Pos is not Set in Switch, ToiletPaper Spawn");

                    Instantiate(PaperPrefab, spawnPos, Quaternion.identity);
                }
            }

            if (ToPlaceEnemysOn.Contains(child.sprite))
            {
                if (Random.Range(0, 100) <= EnemyDensity)
                {
                    Vector3 spawnPos = Vector3.zero;

                    switch (child.sprite.ToString())
                    {
                        case "Tisch (UnityEngine.Sprite)":
                            spawnPos = child.transform.position + new Vector3(0.3f, -0.57f, 0);
                            SetZPositionTo(ref spawnPos, -6);
                            break;
                        case "office_cube2 (UnityEngine.Sprite)":
                            spawnPos = child.transform.position + new Vector3(3.85f, 0.87f, 0);
                            SetZPositionTo(ref spawnPos, -6);
                            break;
                        case "Basic_Background (UnityEngine.Sprite)":
                            spawnPos = child.transform.position + new Vector3(-1.24f, -2.2f, 0);
                            SetZPositionTo(ref spawnPos, -6);
                            break;
                        case "lamp3 (UnityEngine.Sprite)":
                            spawnPos = child.transform.position + new Vector3(0, 3, 0);
                            SetZPositionTo(ref spawnPos, -6);
                            break;
                        case "Basic_Background2 (UnityEngine.Sprite)":
                            spawnPos = child.transform.position + new Vector3(-1.24f, -2.2f, 0);
                            SetZPositionTo(ref spawnPos, -6);
                            break;
                    }

                    if (spawnPos == Vector3.zero)
                       throw new System.Exception("Spawn Pos is not Set in Switch, EnemySpawn");

                    GameObject toSpawn = null;

                    if (partCounter <= LevelLength / 4)
                        toSpawn = Enemy1;
                    else if (partCounter <= LevelLength / 2.5f)
                        toSpawn = Enemy2;
                    else if (partCounter <= LevelLength / 1.2f)
                        toSpawn = Enemy3;
                    else
                        toSpawn = Enemy4;

                    if (toSpawn == null)
                        throw new System.Exception("Enemy To Spawn is Null!");

                    Instantiate(toSpawn, spawnPos, Quaternion.identity);
                }
            }
        }

        GameObject lastGameObject = null;

        foreach (SpriteRenderer sprite in backGrounds)
        {
            if (lastGameObject == null)
                lastGameObject = sprite.gameObject;

            if (sprite.bounds.max.x > lastGameObject.GetComponent<SpriteRenderer>().bounds.max.x)
                lastGameObject = sprite.gameObject;
        }

        return new Vector3(lastGameObject.GetComponent<SpriteRenderer>().bounds.max.x, lastGameObject.GetComponent<SpriteRenderer>().bounds.min.y, StartBackGround.transform.position.z);
    }

    private Vector3 GetCurrentSpirtePositionDownLeft(GameObject newGameObject)
    {
        SpriteRenderer[] childs = new SpriteRenderer[100];
        List<SpriteRenderer> backGrounds = new List<SpriteRenderer>();

        childs = newGameObject.GetComponentsInChildren<SpriteRenderer>();

        foreach (SpriteRenderer child in childs)
        {
            if (child.sprite == BackGround1 || child.sprite == BackGround2 || child.sprite == EndBackGround)
                backGrounds.Add(child);
        }

        GameObject firstGameObject = null;

        foreach (SpriteRenderer sprite in backGrounds)
        {
            if (firstGameObject == null)
                firstGameObject = sprite.gameObject;

            if (sprite.bounds.max.x < firstGameObject.GetComponent<SpriteRenderer>().bounds.max.x)
                firstGameObject = sprite.gameObject;
        }

        return new Vector3(firstGameObject.GetComponent<SpriteRenderer>().bounds.min.x, firstGameObject.GetComponent<SpriteRenderer>().bounds.min.y, StartBackGround.transform.position.z); 
    }

    private void MoveToCorrectPlace(ref GameObject part)
    {
        Vector3 current = GetCurrentSpirtePositionDownLeft(currentAddedPart);
        Vector3 last = GetLastGameObjectPositionDownRight(lastObject);

        Vector3 offset = last - current;
        part.transform.position += offset;

        lastObject = part;
    }

    private void SetZPositionTo(ref Vector3 vector, float ZPos)
    {
        Vector3 tempVector = vector;
        tempVector.z = ZPos;
        vector = tempVector;
    }
}
