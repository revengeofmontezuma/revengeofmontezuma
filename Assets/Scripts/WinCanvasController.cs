﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class WinCanvasController : MonoBehaviour
{
    public BossAI Boss;
    public Image CreditsImage;
    public float CreditsSpeed = 1;
    public float TimeToGoToMainMenu = 30;

    public AudioSource WinSound;

    private Health bossHealth;
    private Canvas canvas;
    private GameManager manager;
    private ChangeScene sceneChange;

    private bool move = false;

    void Awake()
    {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        sceneChange = manager.GetComponent<ChangeScene>();
        bossHealth = Boss.GetComponent<Health>();
        canvas = GetComponentInParent<Canvas>();
    }

    void Start()
    {
        bossHealth.OnDeath += BossHealth_OnDeath;
        canvas.enabled = false;
    }

    void Update()
    {
        if (move)
            CreditsImage.transform.Translate(Vector3.down * -CreditsSpeed);
    }

    private void BossHealth_OnDeath()
    {
        canvas.enabled = true;
        move = true;
        WinSound.Play();
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(TimeToGoToMainMenu);
        sceneChange.ChangeToScene("MainMenu");
    }
}
