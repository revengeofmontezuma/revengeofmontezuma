﻿using UnityEngine;
using UnityEngine.UI;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class SoundAdjust : MonoBehaviour
{
    public Slider MySlider;

    public bool Effect;
    public bool Music;

    private AudioSource[] myAudioSource;

    void Awake()
    {
        myAudioSource = GetComponents<AudioSource>();

        if (Effect && Music)
            throw new System.Exception("Only Select Music or Effect!");

        if (Effect)
            MySlider = GameObject.Find("PauseMenuCanvas/EffectsSlider").GetComponent<Slider>();
        else if (Music)
            MySlider = GameObject.Find("PauseMenuCanvas/MusicSlider").GetComponent<Slider>();
        else
            throw new System.Exception("You Have to Select if Music or Effect!");

        if (GameStats.MusicLoudness != -1 && Music)
        {
            MySlider.value = GameStats.MusicLoudness;
            foreach (var sound in myAudioSource)
                sound.volume = MySlider.value;
        }
        else if (GameStats.EffectLoudness != -1 && Effect)
        {
            MySlider.value = GameStats.EffectLoudness;
            foreach (var sound in myAudioSource)
                sound.volume = MySlider.value;
        }
    }

    void Start()
    {
        MySlider.onValueChanged.AddListener(delegate { OnValueChanged(); });
    }
    
    private void OnValueChanged()
    {
        foreach (var sound in myAudioSource)
            sound.volume = MySlider.normalizedValue;

        if (Effect)
            GameStats.EffectLoudness = myAudioSource[0].volume;
        if (Music)
            GameStats.MusicLoudness = myAudioSource[0].volume;
    }
}
