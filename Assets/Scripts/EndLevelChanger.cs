﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class EndLevelChanger : MonoBehaviour
{
    public string LevelToChangeTo;

    private Transform player;
    private GameManager manager;

    void Awake()
    {
        player = GameObject.Find("Player_Normal").GetComponent<Transform>();
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) <= 4)
            manager.GetComponent<ChangeScene>().ChangeToScene(LevelToChangeTo);
    }
}
