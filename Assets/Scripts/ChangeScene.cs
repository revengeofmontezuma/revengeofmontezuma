﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                             Daniel Mielnicki                                  *    
//********************************************************************************

public class ChangeScene : MonoBehaviour 
{
    public bool ChangeSceneOnWake = false;
    public bool WaitWithLoad;
    public float Dealay;

    public string SceneToLoadOnWake;

    public void Start()
    {
        if (ChangeSceneOnWake)
        {
            if (Application.loadedLevelName == "AutoLoadNextLevel" && WaitWithLoad)
            {
                StartCoroutine(Timer());
            }
            else
                SceneManager.LoadScene(SceneToLoadOnWake);
        }
    }
    
	public void ChangeToScene (string sceneToChangeTo) 
    {
        SceneManager.LoadScene(sceneToChangeTo);
    }

    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(Dealay);
        SceneManager.LoadSceneAsync(SceneToLoadOnWake);
    }
}
