﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class Health : MonoBehaviour
{
    public delegate void DeathDelegate();
    public event DeathDelegate OnDeath;

    public delegate void ChangedDelegate();
    public event ChangedDelegate OnHealthChanged;

    public bool GodMode = false;

    public int MaxHealth;

    public bool RemoveOnDeath;
    public float RemoveDelay = 2;

    public AudioSource RemoveHearthSound;

    [HideInInspector]
    public int CurrentHealth;
    [HideInInspector]
    public bool Alive;

    void Start()
    {
        CurrentHealth = MaxHealth;
        Alive = true;
    }

    public void AddHealth(int health)
    {
        CurrentHealth += health;

        if (CurrentHealth > MaxHealth)
            CurrentHealth = MaxHealth;
    }

    public void RemoveHealth(int damage)
    {
        if (GodMode)
            return;

        if (PlayerPrefs.GetInt("ExtraHeart") > 0 && gameObject.tag == "Player")
        {
            PlayerPrefs.SetInt("ExtraHeart", PlayerPrefs.GetInt("ExtraHeart") - 1);
            RemoveHearthSound.Play();
            if (OnHealthChanged != null)
                OnHealthChanged();

            return;
        }

        if (gameObject.tag == "Boss")
        {
            if (OnHealthChanged != null)
                OnHealthChanged();
        }

        CurrentHealth -= damage;

        if (CurrentHealth <= 0 && Alive)
        {
            CurrentHealth = 0;

            if (OnDeath != null)
                OnDeath();

            if (RemoveOnDeath)
                StartCoroutine(RemoveTimer());

            Alive = false;
        }
    }

    IEnumerator RemoveTimer()
    {
        yield return new WaitForSeconds(RemoveDelay);
        Destroy(gameObject);
    }
}
