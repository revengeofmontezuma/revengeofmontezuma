﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class ColorFadeOut : MonoBehaviour
{
    public bool DeleteOnFadedOut;

    public float TimeToStartFadeOut;
    public float TimeToFadeOut;
    public int FadeOutSteps;

    private SpriteRenderer mySprite;

    void Awake()
    {
        mySprite = GetComponent<SpriteRenderer>();
        StartCoroutine(FadeTimer());

        if (DeleteOnFadedOut)
            StartCoroutine(DeleteTimer());
    }

    IEnumerator DeleteTimer()
    {
        yield return new WaitForSeconds(TimeToStartFadeOut + TimeToFadeOut);
        Destroy(this.gameObject);
    }

    IEnumerator FadeTimer()
    {
        yield return new WaitForSeconds(TimeToStartFadeOut);
        StartCoroutine(FadeTimerFading());
    }

    IEnumerator FadeTimerFading()
    {
        yield return new WaitForSeconds(TimeToFadeOut / FadeOutSteps);

        Color tempColor = mySprite.color;
        tempColor.a -= (float)1 / FadeOutSteps;
        mySprite.color = tempColor;

        StartCoroutine(FadeTimerFading());
    }
}
