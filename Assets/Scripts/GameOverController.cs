﻿using UnityEngine;
using UnityEngine.UI;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class GameOverController : MonoBehaviour
{
    public PlayerController2D PlayerController;

    private Canvas gameOverScreen;
    private Health health;

    void Start()
    {
        gameOverScreen = GetComponent<Canvas>();
        health = PlayerController.GetComponent<Health>();

        gameOverScreen.enabled = false;
        health.OnDeath += Health_OnDeath;
    }

    private void Health_OnDeath()
    {
        gameOverScreen.enabled = true;
    }
}
