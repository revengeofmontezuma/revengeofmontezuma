﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class BackGroundLayer : MonoBehaviour 
{
    public GameObject BackGroundPrefab;

    public Vector2 Speed;
    public float PositionZ;

    [HideInInspector]
    public Dictionary<int, GameObject> Images = new Dictionary<int, GameObject>();

    [HideInInspector]
    public GameObject Left;
    [HideInInspector]
    public GameObject Middle;
    [HideInInspector]
    public GameObject Right;


    public void FillDictionary()
    {
        for (int i = 0; i < 3; i++)
        {
            Images.Add(i, BackGroundPrefab);
        }
    }
}
