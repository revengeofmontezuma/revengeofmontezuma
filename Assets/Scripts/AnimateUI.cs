﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class AnimateUI : MonoBehaviour
{
    public float Delay;
    public List<Sprite> Images = new List<Sprite>();

    private Image myImage;

    private int counter = 0;

    void Awake()
    {
        myImage = GetComponent<Image>();
    }

    void Start()
    {
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(Delay);

        myImage.sprite = Images[counter];

        counter++;

        if (counter >= Images.Count)
            counter = 0;

        StartCoroutine(Timer());
    }
}
