﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

[RequireComponent(typeof(BoxCollider2D))]
public class PickUpScript : MonoBehaviour
{
    [Tooltip("CaseSensitive")]
    public string PlayerName = "Player";


    public int MoneyValue = 5;

    private ParticleSystem myParticle;
    private GameObject TargetToGetPickedUpFrom;
    private SpriteRenderer mySprite;
    private SpriteRenderer targetSprite;
    private GameManager manager;
    private Animator animator;
    private BoxCollider2D myCollider;
    private AudioSource myAudio;

    private float distanceFromCenterX;
    private float distanceFromCenterY;

    private float targetDistanceFromCenterX;
    private float targetDistanceFromCenterY;

    private bool CheckCollision = true;

    void Awake()
    {
        TargetToGetPickedUpFrom = GameObject.Find(PlayerName);
        mySprite = GetComponent<SpriteRenderer>();
        targetSprite = TargetToGetPickedUpFrom.GetComponent<SpriteRenderer>();
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        animator = GetComponent<Animator>();
        myCollider = GetComponent<BoxCollider2D>();
        myParticle = GetComponent<ParticleSystem>();
        myAudio = GetComponent<AudioSource>();
    }

    void Start()
    {
        distanceFromCenterX = mySprite.bounds.extents.x;
        distanceFromCenterY = mySprite.bounds.extents.y;

        targetDistanceFromCenterX = targetSprite.bounds.extents.x;
        targetDistanceFromCenterY = targetSprite.bounds.extents.y;
    }

    void Update()
    {
        if (!CheckCollision)
            return;

        if (manager.Gametime == 1)
        {
            CheckCollisions();
            animator.enabled = true;
        }
        else
            animator.enabled = false;
    }

    private void CheckCollisions()
    {
        if (Mathf.Abs(transform.position.x - TargetToGetPickedUpFrom.transform.position.x) <= distanceFromCenterX + targetDistanceFromCenterX)
        {
            if (Mathf.Abs(transform.position.y - TargetToGetPickedUpFrom.transform.position.y) <= distanceFromCenterY + targetDistanceFromCenterY)
                Collision();
        }

    }

    private void Collision()
    {
        myParticle.Play();
        myAudio.Play();

        mySprite.enabled = false;
        animator.enabled = false;
        myCollider.enabled = false;

        StartCoroutine(DeleteTimer());
        TargetToGetPickedUpFrom.GetComponent<Currency>().AddMoney(MoneyValue);
        CheckCollision = false;
    }

    IEnumerator DeleteTimer()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
