﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

[RequireComponent(typeof(ChangeScene))]
public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public int Gametime = 1;

    void Awake()
    {
        GameStats.Right = KeyCode.A;
        GameStats.Left = KeyCode.D;
        GameStats.Jump = KeyCode.Space;
        GameStats.Attack = KeyCode.Mouse0;
    }
}
