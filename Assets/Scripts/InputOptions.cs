﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class InputOptions : MonoBehaviour
{
    public Dropdown Jump;
    public Dropdown Left;
    public Dropdown Right;
    public Dropdown Attack;

    void Start()
    {
        Jump.onValueChanged.AddListener(delegate { OnChangedJump(); });
        Left.onValueChanged.AddListener(delegate { OnChangedLeft(); });
        Right.onValueChanged.AddListener(delegate { OnChangedRight(); });
        Attack.onValueChanged.AddListener(delegate { OnChangedAttack(); });
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Canvas canvas = gameObject.transform.parent.gameObject.GetComponent<Canvas>();
            canvas.enabled = false;
            gameObject.transform.parent.gameObject.SetActive(false);
        }
    }

    private void OnChangedRight()
    {
        if (Right.value == 0)
            GameStats.Left = KeyCode.D;
        if (Right.value == 1)
            GameStats.Left = KeyCode.RightArrow;
        if (Right.value == 2)
            GameStats.Left = KeyCode.RightArrow;
    }

    private void OnChangedLeft()
    {
        if (Left.value == 0)
            GameStats.Right = KeyCode.A;
        if (Left.value == 1)
            GameStats.Right = KeyCode.LeftArrow;
        if (Left.value == 2)
            GameStats.Right = KeyCode.LeftArrow;
    }

    private void OnChangedJump()
    {
        if (Jump.value == 0)
            GameStats.Jump = KeyCode.Space;
        if (Jump.value == 1)
            GameStats.Jump = KeyCode.W;
        if (Jump.value == 2)
            GameStats.Jump = KeyCode.UpArrow;
    }

    private void OnChangedAttack()
    {
        if (Attack.value == 0)
            GameStats.Attack = KeyCode.Mouse0;
        if (Attack.value == 1)
            GameStats.Attack = KeyCode.E;
        if (Attack.value == 2)
            GameStats.Attack = KeyCode.LeftAlt;
    }
}
