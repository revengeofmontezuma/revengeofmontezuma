﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public static class GameStats
{
    public static int Money { get; set; }

    public static KeyCode Jump;
    public static KeyCode Left;
    public static KeyCode Right;
    public static KeyCode Attack;

    public static float MusicLoudness = -1;
    public static float EffectLoudness = -1;
    
    public static bool BaughtTacker { get; set; }
}
