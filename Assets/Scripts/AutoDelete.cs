﻿using UnityEngine;
using System.Collections;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                             Daniel Mielnicki                                  *    
//********************************************************************************

public class AutoDelete : MonoBehaviour
{
    public float LifeTime;

    void Start()
    {
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(LifeTime);
        Destroy(gameObject);
    }
}
