﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//********************************************************************************
//                                 Author                                        *
//                                                                               *
//                              Lukas Schuster                                   *
//********************************************************************************

public class WeaponChoose : MonoBehaviour
{
    [Header("Colors")]
    public Color NotBaughtColor;
    public Color EquippedColor;

    [Header("Texts")]
    public Text MoneyText;
    public Text AmmoText;

    public Text AmmoPriceText;
    public Text ExtraHeartPriceText;
    public Text TackerPriceText;
    public Text CutterPriceText;

    [Header("Buttons")]
    public Button StandardButtonBuy;
    public Button TackerButtonBuy;
    public Button CutterButtonBuy;
    public Button AmmoButtonBuy;
    public Button ExtraHeartBuy;

    public Button StandardButtonEquip;
    public Button TackerButtonEquip;
    public Button CutterButtonEquip;

    [Header("Data")]
    public int CutterPrice;
    public int TackerPrice;
    public int AmmoPrice;
    public int ExtraHeartPrice;

    [Header("Images")]
    public Image AmmoImage;
    public Image Heart1;
    public Image Heart2;
    public Image Heart3;

    private Color standardButtonStartCollor;
    private Color tackerButtonStartCollor;
    private Color cutterButtonStartCollor;

    private Color showColor;

    void Awake()
    {
        standardButtonStartCollor = StandardButtonBuy.GetComponentInParent<Image>().color;
        tackerButtonStartCollor = TackerButtonBuy.GetComponentInParent<Image>().color;
        cutterButtonStartCollor = CutterButtonBuy.GetComponentInParent<Image>().color;
    }

    void Start()
    {
        AmmoPriceText.text = AmmoPrice.ToString();
        ExtraHeartPriceText.text = ExtraHeartPrice.ToString();
        TackerPriceText.text = TackerPrice.ToString();
        CutterPriceText.text = CutterPrice.ToString();

        StandardButtonBuy.onClick.AddListener(delegate{ StandardClicked(); });
        TackerButtonBuy.onClick.AddListener(delegate { TackerClicked(); });
        CutterButtonBuy.onClick.AddListener(delegate { CutterClicked(); });
        AmmoButtonBuy.onClick.AddListener(delegate { AmmoClicked(); });
        ExtraHeartBuy.onClick.AddListener(delegate { ExtraHeartClicked(); });

        StandardButtonEquip.onClick.AddListener(delegate { StandardEquipClicked(); });
        TackerButtonEquip.onClick.AddListener(delegate { TackerEquipClicked(); });
        CutterButtonEquip.onClick.AddListener(delegate { CutterEquipClicked(); });

        showColor = Heart1.color;

        DisplayHearts();

        switch (PlayerPrefs.GetString("currentWeapon"))
        {
            case "Standard":
                HighlightEquippedWeapon(StandardButtonBuy);
                break;
            case "Tacker":
                HighlightEquippedWeapon(TackerButtonBuy);
                break;
            case "Cutter":
                HighlightEquippedWeapon(CutterButtonBuy);
                break;
            default:
                HighlightEquippedWeapon(StandardButtonBuy);
                break;
        }

        if (PlayerPrefs.GetInt("TackerBought") == 0)
        {
            TackerButtonBuy.transform.parent.GetComponent<Image>().color = NotBaughtColor;
            AmmoImage.color = NotBaughtColor;
        }
        else
            AmmoText.text = PlayerPrefs.GetInt("Ammo").ToString();

        if (PlayerPrefs.GetInt("CutterBought") == 0)
            TackerButtonBuy.transform.parent.GetComponent<Image>().color = NotBaughtColor;
    }

    private void ExtraHeartClicked()
    {
        if (PlayerPrefs.GetInt("money") >= ExtraHeartPrice && PlayerPrefs.GetInt("ExtraHeart") < 3)
        {
            GameStats.Money -= ExtraHeartPrice;
            MoneyText.text = GameStats.Money.ToString();
            PlayerPrefs.SetInt("money", GameStats.Money);

            PlayerPrefs.SetInt("ExtraHeart", PlayerPrefs.GetInt("ExtraHeart") + 1);

            DisplayHearts();
        }
    }

    private void AmmoClicked()
    {
        if (PlayerPrefs.GetInt("money") >= AmmoPrice)
        {
            GameStats.Money -= AmmoPrice;
            MoneyText.text = GameStats.Money.ToString();
            PlayerPrefs.SetInt("money", GameStats.Money);

            PlayerPrefs.SetInt("Ammo", PlayerPrefs.GetInt("Ammo") + 10);
        }

        AmmoText.text = PlayerPrefs.GetInt("Ammo").ToString();
    }

    private void CutterEquipClicked()
    {
        if (PlayerPrefs.GetInt("CutterBought") == 1)
        {
            SetCurrentWeapon("Cutter");
            HighlightEquippedWeapon(CutterButtonBuy);
        }
    }

    private void TackerEquipClicked()
    {
        if (PlayerPrefs.GetInt("TackerBought") == 1)
        {
            SetCurrentWeapon("Tacker");
            HighlightEquippedWeapon(TackerButtonBuy);
        }
    }

    private void StandardEquipClicked()
    {
        SetCurrentWeapon("Standard");
        HighlightEquippedWeapon(StandardButtonBuy);
    }

    private void StandardClicked()
    {

    }

    private void TackerClicked()
    {
        if (GameStats.Money >= TackerPrice && PlayerPrefs.GetInt("TackerBought") == 0)
        {
            GameStats.Money -= TackerPrice;
            MoneyText.text = GameStats.Money.ToString();
            PlayerPrefs.SetInt("money", GameStats.Money);

            TackerButtonBuy.transform.parent.GetComponent<Image>().color = tackerButtonStartCollor;
            AmmoImage.color = tackerButtonStartCollor;
            PlayerPrefs.SetInt("TackerBought", 1);
        }
    }

    private void CutterClicked()
    {
        if (GameStats.Money >= TackerPrice && PlayerPrefs.GetInt("CutterBought") == 0)
        {
            GameStats.Money -= CutterPrice;
            MoneyText.text = GameStats.Money.ToString();
            PlayerPrefs.SetInt("money", GameStats.Money);

            CutterButtonBuy.transform.parent.GetComponent<Image>().color = cutterButtonStartCollor;
            PlayerPrefs.SetInt("CutterBought", 1);
        }
    }

    private void SetCurrentWeapon(string weapon)
    {
        PlayerPrefs.SetString("currentWeapon", weapon);
    }

    private void HighlightEquippedWeapon(Button toHighlight)
    {
        StandardButtonBuy.transform.parent.GetComponent<Image>().color = standardButtonStartCollor;

        TackerButtonBuy.transform.parent.GetComponent<Image>().color = PlayerPrefs.GetInt("TackerBought") == 1 ? tackerButtonStartCollor : NotBaughtColor;
        CutterButtonBuy.transform.parent.GetComponent<Image>().color = PlayerPrefs.GetInt("CutterBought") == 1 ? cutterButtonStartCollor : NotBaughtColor;

        toHighlight.transform.parent.GetComponent<Image>().color = EquippedColor;
    }

    private void DisplayHearts()
    {
        switch (PlayerPrefs.GetInt("ExtraHeart"))
        {
            case 0:
                ResetAllheartExept(new List<Image>() { });
                break;
            case 1:
                ResetAllheartExept(new List<Image>() { Heart1 });
                break;
            case 2:
                ResetAllheartExept(new List<Image>() { Heart1, Heart2 });
                break;
            case 3:
                ResetAllheartExept(new List<Image>() { Heart1, Heart2, Heart3 });
                break;
        }
    }

    private void ResetAllheartExept(List<Image> hearts)
    {
        Color heart1Color = Heart1.color;
        heart1Color.a = 0;
        Heart1.color = heart1Color;

        Color heart2Color = Heart2.color;
        heart2Color.a = 0;
        Heart2.color = heart2Color;

        Color heart3Color = Heart3.color;
        heart3Color.a = 0;
        Heart3.color = heart3Color;


        foreach (var image in hearts)
            image.color = showColor;
    }

    private void ResetPlayerPrefs()
    {
        SetCurrentWeapon("Standard");
        PlayerPrefs.SetInt("Ammo", 0);
        PlayerPrefs.SetInt("TackerBought", 0);
        PlayerPrefs.SetInt("CutterBought", 0);
    }
}
